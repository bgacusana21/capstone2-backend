const mongoose = require('mongoose')
const moment = require('moment')

const transactionCategorySchema = new mongoose.Schema({
    id: {
        type: String,
        // required: [true, 'Transaction type is required']
    },
    type: {
        type: String,
        // required: [true, 'Transaction subcatetory is required']
    },
    value: {
        type: String,
        // required: [true, 'Transaction subcatetory value is required']
    },
    categorySelection: {
        type: String,
        // required: [true, 'Transaction subcatetory value is required']
    }
})

module.exports = mongoose.model('transactionCategory', transactionCategorySchema)