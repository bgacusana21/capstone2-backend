const mongoose = require('mongoose')
const moment = require('moment')

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name is required']
    },
    lastName: {
        type: String,
        required: [true, 'Last name is required']
    },
    mobileNo: {
        type: String,
        //required: [true, 'Mobile number is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    loginType: {
        type: String,
        required: [true, 'login type is required']
    },
    password: {
        type: String,
        //required: [true, 'Password is required']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    userStatus: {
        type: String,
        default: 'Active'
    },
    transaction: [
        {
            transactionType: {
                type: String,
                required: [true, 'Transaction type is required']
            },
            transactSubCat: {
                type: String,
                required: [true, 'Transaction subcatetory is required']
            },
            transactionValue: {
                type: Number,
                default: 0
            },
            isNegative: {
                type: Boolean,
                default: false
            },
            transactionTimestamp: {
                type: Date,
                default: moment()
            }
        }
    ]
})

module.exports = mongoose.model('user', userSchema)